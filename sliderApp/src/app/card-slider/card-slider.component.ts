import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-card-slider',
  templateUrl: './card-slider.component.html',
  styleUrls: ['./card-slider.component.scss']
})
export class CardSliderComponent implements OnInit {

  cards = [
    {
      name: 'Mobile Internet',
      isActive: false,
      direction: 'prev'
    },
    {
      name: 'Home Internet',
      isActive: false,
      direction: 'prev'
    },
    {
      name: 'Get a Device',
      isActive: true,
      direction: ''
    },
    {
      name: 'Add a phoneline',
      isActive: false,
      direction: 'next'
    },
    {
      name: 'Upgrade',
      isActive: false,
      direction: 'next'
    }
  ];

  activeIndex = 2;

  constructor() { }

  ngOnInit(): void {
  }

  move(direction: string){
    let preActiveIndex = this.activeIndex;
    this.cards[this.activeIndex].isActive = false;
    this.cards.map(c => c.direction = '');
  
    if(direction === 'prev'){
      this.activeIndex = this.activeIndex - 1;
      this.cards[this.activeIndex].isActive = true;
    } else {
      this.activeIndex = this.activeIndex + 1;
      this.cards[this.activeIndex].isActive = true;
    }

    this.cards.map((c, i) => {
      c.direction = (i < this.activeIndex) ? 'prev' : ((i > this.activeIndex) ? 'next' : '');
    });
  }

}
